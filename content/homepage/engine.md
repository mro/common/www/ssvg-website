---
title: "The SSVG Engine"
weight: 5
header_menu: true
---

A reference implementation of a **SSVG Engine** has been made and is available on {{< extlink text="CERN GitLab" href="https://gitlab.cern.ch/mro/common/www/ssvg-engine/-/blob/master/README.md" icon="fa fa-gitlab title" >}} {{<icon class="fa fa-exclamation title">}}


A **pre-built** version of the SSVG Engine is available {{<extlink text="here" href="https://ssvg.web.cern.ch/distfiles/js/ssvg-engine-1.1.js" icon="fa fa-nodejs title" download="1">}}.

{{<icon class="fa fa-asterisk title">}}
**Note:** This **SSVG Engine** implementation is designed to have as **few dependencies** as possible and to be loaded as a **plugin** in browsers and web-pages, it mainly relies on {{< extlink text="D3.js" href="https://d3js.org/" >}}.

{{<icon class="fa fa-asterisk title">}}
**Note:** there may be several implementations of **SSVG Engines**, any library that implements the {{< extlink text="SSVG Specification" href="https://edms.cern.ch/ui/file/2417668/1/ssvg-specification.pdf" >}} can be named as such.
