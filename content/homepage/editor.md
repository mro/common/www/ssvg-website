---
title: "The SSVG editor"
weight: 6
header_menu: true
---

An on-line **SSVG Editor** is available {{< extlink href="https://ssvg.web.cern.ch/ssvg-editor" text="here" >}}{{<icon class="fa fa-exclamation title">}}

{{<icon class="fa fa-asterisk title">}}
It embeds its own **online documentation**, check-out for {{<icon class="fa fa-question-circle title">}} icons in the app.

{{<icon class="fa fa-asterisk title">}}
An additional **user-guide** is also available here {{< extlink text="CERN/EDMS 2820020" href="https://edms.cern.ch/ui/file/2820020/1/ssvg-editor-user-guide.pdf" >}}
(and {{< extlink href="https://ssvg.web.cern.ch/docs/bcp/ssvg-editor-user-guide.html" text="there">}}).
