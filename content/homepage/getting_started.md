---
title: "Getting Started"
weight: 6
header_menu: true
---

[{{< icon class="fa fa-graduation-cap title">}}](/examples)
To **discover** and **learn** about SSVG a lot of [**examples**](/examples) are available.

[{{< icon class="fa fa-edit title">}}](#the-ssvg-editor)
To **prepare** a SSVG document the [**SSVG editor**](#the-ssvg-editor) is recommended.

{{<rawhtml>}}

{{<icon class="fa fa-cogs title">}}
{{<markdownify>}}To **integrate** it several tools are available:{{</markdownify>}}

<div style="margin-left: 2em">
{{<details summary="As a **browser custom-element plugin**, automatically loading `s-svg` elements:" raw="1">}}
<a style="float: right;position: absolute; right: 5px;" title="Open and test" target="_blank" href="/integration/ssvg-plugin.html"><i class="fa fa-external-link title"></i></a>
{{< code file="/static/integration/ssvg-plugin.html" language="HTML" >}}
{{</details>}}

{{<details summary="As a regular browser **library**:" raw="1">}}
<a style="float: right;position: absolute; right: 5px;" title="Open and test" target="_blank" href="/integration/ssvg-library.html"><i class="fa fa-external-link title"></i></a>
{{<code file="/static/integration/ssvg-library.html" language="HTML">}}
{{</details>}}

{{<details summary="In a Node.js project as a **dependency**:" raw="1">}}
{{<code file="/static/integration/ssvg-install.sh" language="bash">}}
{{<code file="/static/integration/ssvg-engine-webpack.js" language="js">}}
{{</details>}}

{{<details summary="In a **Vue.js** framework:" raw="1">}}
<a style="float: right;position: absolute; right: 5px;" title="Open and test" target="_blank" href="/integration/ssvg-vue.html"><i class="fa fa-external-link title"></i></a>
{{<code file="/static/integration/ssvg-vue.html" language="HTML">}}
{{</details>}}


</div>
{{</rawhtml>}}
