---
title: "What is SSVG ?"
header_menu_title: "What is SSVG ?"
navigation_menu_title: "about"
weight: 1
header_menu: true
---

**S**tateful **S**calar **V**ector **G**raphics (namely SSVG) brings a **logical state** to a **graphical document**.

It is an **extension to {{< extlink text="SVG" href="https://www.w3.org/TR/SVG11/" >}}** that relies on a **declarative syntax** to describe how the document should **transform** and **evolve** according to input values.

It is designed to be **as simple as possible**, with three different type of elements:

1. **properties** declaration that describes your documment current state.
2. **relations** and **transformations** that link the properties to the document graphical elements.
3. Optional **transitions** to describe on how changes should be reflected.

Once this **state** description is embedded in a {{< extlink text="SVG" href="https://www.w3.org/TR/SVG11/" >}}, it may be loaded by an **SSVG-Engine** that will transform your graphical document into a complete **reactive object** that you can feed with values and that knows how to **render** itself and **display** changes {{<icon class="fa fa-exclamation title">}}

{{<icon class="fa fa-asterisk title">}}
The complete **RFC specification** is available here {{< extlink text="CERN/EDMS 2417668" href="https://edms.cern.ch/ui/file/2417668/1/ssvg-specification.pdf" >}} (or {{< extlink text="there" href="https://ssvg.web.cern.ch/docs/std/ssvg-specification.html" >}}).
