---
title: "Example: animated emoji transforms"
navigation_menu_title: "Example: transforms"
weight: 3
header_menu: false
tags: [ CSS, transformations ]
image: "/images/surprise_emoji.svg"
---

This detailed **SSVG** graphics example demonstrates CSS and XML **transformations** by animating the following emoji:

{{<rawhtml>}}

<style>

    #emoji-ssvg, #emoji-logo-ssvg {
        margin-top: 5px;
        margin-bottom: 5px;
        width:25vw;
        max-height:25vh;
    }
    @media only screen and (max-width: 500px) {
        #emoji-ssvg, #emoji-logo-ssvg {
            max-height:50vh;
        }
    }
</style>
<div style="display:flex;justify-content:space-evenly;">
<s-svg id="emoji-ssvg">
    {{< include "/static/images/surprise_emoji.svg" >}}
</s-svg>
<s-svg id="emoji-logo-ssvg">
    {{< include "/static/images/logo.svg" >}}
</s-svg>
</div>

<i class="fa fa-hand-o-right title"></i> Click the <strong>button</strong> to modify modify the SVG state:
<button type="button" id="emoji-input">Surprise me !</button>

{{</rawhtml>}}

{{<icon class="fa fa-flask title">}}
The first step is to describe `properties` to represent the gauge logical `state`, for such an item a single
**surprised** boolean `property` seems appropriate.

{{<icon class="fa fa-wrench title">}}
Then the `transforms` to apply to the graphical element depending on its `state` must be described, in this specific example we will **translate** the emoji's **pupils** and **mouth**.

{{<icon class="fa fa-wrench title">}}
Let's add a `transition` to smoothly animate our graphical item.

Describing those `properties` and `transforms` in a **declarative** way is exactly what **SSVG** is meant for {{<icon class="fa fa-exclamation title">}}

{{<details summary="The following elements may be **embedded** in the gauge SVG document, evolving it as an **SSVG** {{<icon class=\"fa fa-magic title\">}} :" open=1 >}}
```XML
<state>
  <!-- this will be the default transition for all transforms and relations -->
  <transition duration=".25s" delay="0.75s" type="strict" timing-function="ease-in-out"></transition>
  <property name="surprise" type="boolean" min="0" max="1" initial="0">
    <!-- pupils are on the right by default, let's transition to the left when not surprised -->
    <transform query-selector=".pupil" attribute-name="transform" attribute-type="CSS"
      from="translate(-2px, 0px)" to="translate(0px, 0px)"></transform>
    <!-- the entire mouth is on the right by defaultm let's transition it on the left when not surprised -->
    <transform query-selector=".mouth" attribute-name="transform" attribute-type="CSS"
      from="translate(-1.7px, 0px)" to="translate(0px, 0px)"></transform>
    <!-- bottom part of the mouth needs to be moved up to close it -->
    <transform query-selector="#mouth-down" attribute-name="transform" attribute-type="CSS"
      from="translate(0px, -2px)" to="translate(0px, 0px)"></transform>
    <!-- top part of the mouth needs to be moved down to close it -->
    <transform query-selector=".mouth-up, #mouth-mid" attribute-name="transform" attribute-type="CSS"
      from="translate(0px, 2px)" to="translate(0px, 0px)"></transform>
    <!-- middle part of the mouth needs to disapear when the mouth is closed -->
    <relation query-selector="#mouth-mid" attribute-name="height" attribute-type="XML"
      from="0" to="3.9575014">
    </relation>
    <!-- let's add a wrinkle, it's apparitong will be slightly delayed using key-times -->
    <relation query-selector="#wrinkle" attribute-name="opacity" attribute-type="CSS"
      values="0;0;1" key-times="0;0.75;1"></relation>
    <!-- enlarge pupils -->
    <relation query-selector=".pupil" attribute-name="ry" attribute-type="XML"
        from="0.7" to="1.2">
      <!-- pupils' size have their own transition, eyes are not so fast -->
      <transition duration="1s" delay="0.75s" type="strict" timing-function="linear"></transition>
    </relation>
  </property>
</state>
```
{{</details>}}

{{<rawhtml>}}
<script>
  function init() {
    window.emojiObject = {
        ssvg: $("#emoji-ssvg"),
        logo: $('#emoji-logo-ssvg'),
        input: $("#emoji-input")
    };

    emojiObject.input.on("click", function () {
        const ssvg = emojiObject.ssvg.get(0);
        const surprise = !ssvg.getState().surprise;
        emojiObject.ssvg.get(0).setState({ surprise: surprise });
        emojiObject.logo.get(0).setState({ range: surprise ? 2 : 0 });
    });
    emojiObject.ssvg.get(0).setState({ surprise: false });
  }
  window.addEventListener("load", init);
</script>
{{</rawhtml>}}

The **SSVG Engine** is reflecting **logical state** changes on the **graphical** elements of the SVG {{<icon class="fa fa-exclamation title">}}

{{<icon class="fa fa-asterisk title">}}
**Note:** the **logical state** is not impacted by `transitions`, it is always up to date, the **graphical elements** are transitioned by the **SSVG Engine** to smoothly reflect changes.

{{<icon class="fa fa-asterisk title">}}
**Note:** `transforms` are quite similar to `relations` unless that it refers to [CSS](https://www.w3.org/TR/css-transforms-1) or [SVG transformations](https://www.w3.org/TR/SVG11/coords.html#TransformAttribute) (using the **attribute-type** **CSS** or **XML** to switch from one to the other).
