---
title: "Example: SSVG logo !"
weight: 4
image: "/images/logo.svg"
tags: [CSS, transformations, 3D]
---

This is a detailed **SSVG** graphics example, demonstrating how to use **CSS3 transformations** animating the following **SSVG logo**:

{{<rawhtml>}}

<div style="display:flex;justify-content:center;">
<s-svg id="ssvg" style="width:25vw;max-height:50vh;">
    {{< include "/static/images/logo.svg" >}}
</s-svg>
</div>
<i class="fa fa-hand-o-right title"></i> Select an <strong>item</strong> to modify the logo's internal state:
<div style="display:flex;justify-content:space-between;padding-left: 2em;padding-right: 2em;margin-bottom: 1em;">
<div>
    <input checked type="radio" id="state0" name="state" value="0" oninput="ssvgObject.ssvg.updateState({ range: event.target.value })"/>
    <label for="state0">Initial</label>
</div>

<div>
    <input type="radio" id="state1" name="state" value="1" oninput="ssvgObject.ssvg.updateState({ range: event.target.value })"/>
    <label for="state1">3D rotation</label>
</div>

<div>
    <input type="radio" id="state2" name="state" value="2" oninput="ssvgObject.ssvg.updateState({ range: event.target.value })"/>
    <label for="state2">Explode</label>
</div>

<div>
    <input type="radio" id="state3" name="state" value="3" oninput="ssvgObject.ssvg.updateState({ range: event.target.value })"/>
    <label for="state3">Final</label>
</div>
</div>
{{</rawhtml>}}

{{<details summary="This example is quite advanced and requires some knowledge on **CSS transformations**, the following external documentation may help:">}}

- {{<extlink href="https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Transforms/Using_CSS_transforms" text="MDN: Using CSS transforms">}}
- {{<extlink href="https://developer.mozilla.org/en-US/docs/Web/CSS/transform" text="MDN: transform">}}
- {{<extlink href="https://www.w3schools.com/cssref/css3_pr_transform.php" text="W3Schools: CSS transform Property">}}
- {{<extlink href="https://www.w3.org/TR/css-transforms-1" text="W3: CSS Transforms Module Level 1">}}
- {{<extlink href="https://www.w3.org/TR/css-transforms-2" text="W3: CSS Transforms Module Level 2">}}
{{</details>}}

{{<icon class="fa fa-cogs title">}}
Three different transforms are applied, on the **shield**, on a **sub-part of the shield** and on the **star**, all of those are described in the `values` attribute of the associated `transform` element in a semi-colon (`;`) separated list of **CSS3 compatible** primitives.

{{<details summary="The following elements may be **embedded** in the SSVG logo document, evolving it as an **SSVG** {{<icon class=\"fa fa-magic title\">}} :" open=1 >}}

```XML
<state>
    <transition duration='750ms' timing-function='ease-in-out' />
    <property initial='0' min='0' max='3' name='range' type='number'>
        <transform query-selector='.ssvg-shield' attribute-type='CSS'
            values='scale(1);scale(.8) rotateX(60deg) rotateZ(-45deg);scale(.8) rotateX(60deg) rotateZ(-10deg) translateZ(60px); scale(1)' />
        <transform query-selector='.ssvg-shield-part' attribute-type='CSS'
            values='scale(1);scale(.8) rotateX(60deg) rotateZ(-45deg);scale(.8) rotateX(60deg) rotateZ(-10deg) translateZ(100px); scale(1)' />
        <transform query-selector='.ssvg-star' attribute-type='CSS'
          values='scale(1);scale(.8) rotateX(60deg) rotateZ(-45deg);scale(.8) rotateX(60deg) rotateZ(-10deg) translateZ(-20px) translateY(-340px); scale(1)' />
    </property>
</state>
```
{{</details>}}

{{<rawhtml>}}
<script>
  function init() {
    window.ssvgObject = {
        ssvg: $("#ssvg").get(0),
        input: $("#input"),
    };
  }
  window.addEventListener("load", init);
</script>
{{</rawhtml>}}

The **SSVG Engine** is reflecting **logical state** changes on the **graphical** elements of the SVG {{<icon class="fa fa-exclamation title">}}

{{<icon class="fa fa-asterisk title">}}
**Note:** the first and last transforms contains a single `scale(1)` item, other transforms will **automatically be appended** with their parameters set to `0`, making it a identity transformation.

{{<icon class="fa fa-asterisk title">}}
**Note:** most CSS transforms requires a unit (like CSS properties) when SVG transforms don't (and have a default native-unit).
