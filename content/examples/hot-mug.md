---
title: "Example: hot-mug computed properties"
weight: 2
image: "/images/hot-mug.svg"
tags: [ simple, computed ]
---

In this **SSVG** graphics example, we would like to add some logic in the SSVG state by using `computed` properties.

{{<rawhtml>}}
<div style="display:flex;justify-content:center;">
<s-svg id="mug-ssvg" style="width:25vw;max-height:25vh;">
    {{< include "/static/images/hot-mug.svg" >}}
</s-svg>

</div>

<div style="display:flex;justify-content:space-between;align-items:center;">
    <div style="width:40%;">
        <i class="fa fa-hand-o-right title"></i> Move the <strong>slider</strong> to modify the SSVG internal state:
        <input style="width:100%;" id="mug-input" type="range" value="10" min="-80" max="180" />
    </div>

    <svg style="width:10%;padding:1rem;">{{< include "/static/images/arrow.svg" >}}</svg>

    <div style="width:40%;">

        <i class="fa fa-exclamation-circle title"></i> The mug's <strong>logical state</strong>:

<pre tabindex="0" class="chroma"><code class="language-json" data-lang="json"><span class="line"><span class="ln">1</span><span class="cl"><span class="p">{</span>
</span></span><span class="line"><span class="ln">2</span><span class="cl">    <span class="nt">"temperature"</span><span class="p">:</span> <span class="mi" id="mug-value">0</span><span class="p">,</span>
</span></span><span class="line"><span class="ln">3</span><span class="cl">    <span class="nt">"hot"</span><span class="p">:</span> <span class="kc" id="mug-hot">false</span><span class="p">,</span>
</span></span><span class="line"><span class="ln">4</span><span class="cl">    <span class="nt">"cold"</span><span class="p">:</span> <span class="kc" id="mug-cold">false</span>
</span></span><span class="line"><span class="ln">5</span><span class="cl"><span class="p">}</span>
</span></span></code></pre>
    </div>
</div>
{{</rawhtml>}}

{{<icon class="fa fa-flask title">}}
The first step is to describe `properties` to represent the mug logical `state`, for such an item a single
**temperature** `property` seems appropriate.

{{<icon class="fa fa-flask title">}}
Then some derivate `computed` properties may be added to augment the mug's logic, adding a **cold** and **hot** `computed` properties seems appropriate ; the logic for those `computed` properties will be implemented in an `onupdate` attribute in JavaScript.

{{<icon class="fa fa-wrench title">}}
Finally the `relation` between this `state` and the graphical element itself must be described, in this specific example we will animate the some elements **opacity** to make **steam** or **ice** appear depending on the temperature.

Describing those `properties` and `relations`, augmenting our graphics with more advanced `computed` logic is exactly what **SSVG** is meant for {{<icon class="fa fa-exclamation title">}}

{{<details summary="The following elements may be **embedded** in the gauge SVG document, evolving it as an **SSVG** {{<icon class=\"fa fa-magic title\">}} :" open=1 >}}
```XML
<state>
    <transition duration="2500ms" timing-function="ease-in-out" type="direct"></transition>
    <property name="temperature" type="number" min="-80" max="180" initial="0"></property>
    <computed name="cold" type="boolean" onupdate="$state.temperature &lt;= 0">
        <relation query-selector="#ice" attribute-name="opacity" from="0" to="1"></relation>
    </computed>
    <computed name="hot" type="boolean" onupdate="$state.temperature &gt;= 40">
        <relation query-selector="#steam" attribute-name="opacity" from="0" to="1"></relation>
    </computed>
</state>
```
{{</details>}}

{{<rawhtml>}}
<script>
  function init() {
    window.ssvgObject = {
        ssvg: $("#mug-ssvg"),
        input: $("#mug-input"),
        value: $("#mug-value"),
        iceOpacity: $("#mug-cold"),
        hotOpacity: $("#mug-hot")
    };

    ssvgObject.input.on("input", function () {
        ssvgObject.ssvg.get(0).setState({ temperature: this.value });
        ssvgObject.value.text(this.value);
        ssvgObject.iceOpacity.text(this.value < 0 ? "true" : "false");
        ssvgObject.hotOpacity.text(this.value >= 40 ? "true" : "false");
    });
    ssvgObject.ssvg.get(0).setState({ temperature: 10 });
    ssvgObject.input.val("10");
    ssvgObject.value.text("10");
  }
  window.addEventListener("load", init);
</script>

{{</rawhtml>}}

The **SSVG Engine** keeping the **logical state** up to date {{<icon class="fa fa-exclamation title">}}

{{<icon class="fa fa-asterisk title">}}
**Note:** computed properties may be implemented as **simple statement** or a **function**, example:
```XML
<computed name="hot" type="boolean" onupdate="function(s) { return s.temperature >= 40; }"></computed>
```

{{<icon class="fa fa-asterisk title">}}
**Note:** computed properties may **depend** on other properties, the computed are evaluated in **declaration order**.
