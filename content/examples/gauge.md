---
title: "Example: animated gauge relations"
weight: 1
image: "/images/gauge.svg"
tags: [simple, CSS, XML, relations]
---

This is a detailed **SSVG** graphics example, we would like to animate the following gauge:

{{<rawhtml>}}

<div style="display:flex;justify-content:center;">
<s-svg id="gauge-ssvg" style="width:25vw;max-height:25vh;">
    {{< include "/static/images/gauge.svg" >}}
</s-svg>
</div>
<i class="fa fa-hand-o-right title"></i> Move the <strong>slider</strong> to modify the gauge's internal state:
<div style="display:flex;justify-content:center;">
<input style="width:25vw;" id="gauge-input" type="range" value="0" min="-40" max="80" />
</div>
{{</rawhtml>}}

{{<icon class="fa fa-flask title">}}
The first step is to describe `properties` to represent the gauge logical `state`, for such an item a single
**temperature** `property` seems appropriate.

{{<icon class="fa fa-wrench title">}}
Then the `relation` between this `state` and the graphical element itself must be described, in this specific example we will animate the gauge's **fill color** and the **fluid level** (a clipping rectangle size).

Describing those `properties` and `relations` in a **declarative** way is exactly what **SSVG** is meant for {{<icon class="fa fa-exclamation title">}}

{{<details summary="The following elements may be **embedded** in the gauge SVG document, evolving it as an **SSVG** {{<icon class=\"fa fa-magic title\">}} :" open=1 >}}

```XML
<state>
    <property name="temperature" type="number" min="-40" max="80" initial="30">
        <!-- a clip reclangle hides parts of the gauge-fluid -->
        <relation query-selector="#clipRect" attribute-name="y" attribute-type="XML"
            values="267;184;32" key-times="0;0.33333;1"></relation>
        <!-- animate the gauge fill color using different colors -->
        <relation query-selector="#gauge"  attribute-name="fill" attribute-type="CSS"
            values="hsl(224, 100%, 53%);#878787;hsl(0, 65%, 53%)"
            key-times="0;0.33333;1"></relation>
    </property>
</state>
```

{{</details>}}

{{<details summary="Now let's have a closer look to what's happening {{<icon class=\"fa fa-cogs title\">}}:" raw=1 open=1 >}}

{{<rawhtml>}}

<div style="display:flex;justify-content:space-between;align-items:center;">

<div style="width:40%;">

<i class="fa fa-exclamation-circle title"></i> The gauge's <strong>logical state</strong>:

<pre style="margin-top: 1rem;" tabindex="0" class="chroma"><code class="language-Json" data-lang="Json"><span class="line"><span class="cl"><span class="p">{</span>
</span></span><span class="line"><span class="cl">    <span class="nt">"temperature"</span><span class="p">:</span> <span id="gauge-value" class="mi">0</span>
</span></span><span class="line"><span class="cl"><span class="p">}</span>
</span></span></code></pre>
</div>

<svg style="width:10%;padding:1rem;">{{< include "/static/images/arrow.svg" >}}</svg>

<div style="width:50%;">
<i class="fa fa-exclamation-circle title"></i> The gauge's <strong>clipRect Y</strong> is modified by the <strong>SSVG Engine</strong>:
<pre style="margin-top: 1rem;" tabindex="0" class="chroma"><code class="language-XML" data-lang="XML"><span class="line"><span class="cl"><span class="nt">&lt;rect</span> <span class="na">id=</span><span class="s">"clipRect"</span> <span class="na">x=</span><span class="s">"32"</span>
    <span class="na">y=</span><span id="gauge-clip-rect-y" class="s">"190.22423224232242"</span>
    <span class="na">width=</span><span class="s">"107.63"</span>
    <span class="na">height=</span><span class="s">"234"</span><span class="nt">&gt;
&lt;/rect&gt;</span>
</span></span></code></pre>

<i class="fa fa-exclamation-circle title"></i> The gauge's <strong>fill color</strong> is modified by the <strong>SSVG Engine</strong>:

<pre style="margin-top: 1rem;" tabindex="0" class="chroma"><code class="language-CSS" data-lang="CSS"><span class="line"><span class="cl"><span class="p">#</span><span class="nn">gauge</span> <span class="p">{</span>
</span></span><span class="line"><span class="cl">    <span class="n">fill</span><span class="p">:</span> <span id="gauge-fill-color" class="mh">#878787</span><span class="p">;</span>
</span></span><span class="line"><span class="cl"><span class="p">}</span>
</span></span></code></pre>
</div>

</div>

<script>
  function init() {
    window.gaugeObject = {
        ssvg: $("#gauge-ssvg"),
        input: $("#gauge-input"),
        value: $("#gauge-value"),
        setClipRectY(value) { $('#gauge-clip-rect-y').text(`"${value}"`); },
        setGaugeFill(value) { $('#gauge-fill-color').text(value); }
    };

    gaugeObject.ssvg.find('state relation:nth-of-type(1)').get(0).onupdate = function(value) {
        gaugeObject?.setClipRectY(value);
        return value;
    }

    gaugeObject.ssvg.find('state relation:nth-of-type(2)').get(0).onupdate = function(value) {
        gaugeObject?.setGaugeFill(value);
        return value;
    }

    gaugeObject.input.on("input", function () {
        gaugeObject.ssvg.get(0).setState({ temperature: this.value });
        gaugeObject.value.text(this.value);
    });
    gaugeObject.ssvg.get(0).setState({ temperature: 20 });
  }
  window.addEventListener("load", init);
</script>

{{</rawhtml>}}
{{</details>}}

The **SSVG Engine** is reflecting **logical state** changes on the **graphical** elements of the SVG {{<icon class="fa fa-exclamation title">}}

{{<icon class="fa fa-asterisk title">}}
**Note:** a relation may target one or more elements using its **query-selector** that contains a [CSS selector](https://www.w3.org/TR/selectors-4).

{{<icon class="fa fa-asterisk title">}}
**Note:** relations' values may be **numbers** (with or without type), **colors** or even a list of **string** values.

{{<icon class="fa fa-asterisk title">}}
**Note:** a relation may target an **XML** attribute or a **CSS** styling property of the target element(s) using its **attribute-type** and **attribute-name**.

{{<icon class="fa fa-asterisk title">}}
**Note:** **key-times** may be used to define a non-linear mapping over the property value.
