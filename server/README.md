# Build

From the project's root directory:

```bash
# To build the container (using podman or docker)
podman build -f server/Dockerfile . -t ssvg-website:xp

# To run the container
podman run --rm -it -p 8080:8080 ssvg-website:xp
```

# Deploy

To deploy the example application, assuming that `oc` is configured and connected
on the proper project:

```bash
# Modify parameters in 'config.env' file properly
# Do never commit passwords/secrets!
oc process --local --parameters -f deploy/ssvg-website.template.yaml # for details about parameters
vim config.env

# Generate the configuration and apply it:
# 1. process the deployment template file into a list of openshift resources
# 2. apply the resulting configuration to a pod
oc process --local --param-file=deploy/config.env -o 'yaml' -f deploy/ssvg-website.template.yaml > deploy/ssvg-website.yaml
oc apply -f deploy/ssvg-website.yaml
```
