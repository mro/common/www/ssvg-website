import { SSVGEngine } from 'ssvg-engine';

/* create and engine hooking it on a DOM element */
const engine = new SSVGEngine(window.getElementById('svg'));

/* update the engine state */
engine.updateState({ range: 12 });

/* dispose the object */
engine.disconnect();